package com.hendisantika.springbootstatisticsapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootStatisticsApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootStatisticsApiApplication.class, args);
	}

}
