package com.hendisantika.springbootstatisticsapi.component;

import com.hendisantika.springbootstatisticsapi.service.TransactionService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-statistics-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/01/20
 * Time: 18.47
 */
@Component
public class TransactionCleaner {

    private TransactionService ts;

    public TransactionCleaner(TransactionService service) {
        this.ts = service;
    }

    @Scheduled(fixedRate = 500)
    public void cleanExpiredTransactions() {
        ts.removeExpired();
    }
}