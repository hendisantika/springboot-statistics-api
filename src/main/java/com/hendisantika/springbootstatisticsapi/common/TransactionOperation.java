package com.hendisantika.springbootstatisticsapi.common;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-statistics-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/01/20
 * Time: 18.49
 */
public enum TransactionOperation {
    INSERT, REMOVE
}