package com.hendisantika.springbootstatisticsapi.common;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-statistics-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/01/20
 * Time: 18.48
 */
@ResponseStatus(value = HttpStatus.GONE, reason = "Transaction already expired.")
public class TransactionExpired extends Exception {
}