package com.hendisantika.springbootstatisticsapi.controller;

import com.hendisantika.springbootstatisticsapi.common.TransactionExpired;
import com.hendisantika.springbootstatisticsapi.domain.Statistic;
import com.hendisantika.springbootstatisticsapi.domain.Transaction;
import com.hendisantika.springbootstatisticsapi.service.TransactionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-statistics-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/01/20
 * Time: 18.54
 */
@RestController
public class TransactionController {

    private TransactionService ts;

    public TransactionController(TransactionService service) {
        this.ts = service;
    }

    @PostMapping("/transactions")
    public ResponseEntity addStatistics(@RequestBody Transaction t) {
        try {
            ts.add(t);
            return new ResponseEntity(HttpStatus.CREATED);
        } catch (TransactionExpired e) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/statistics")
    public Statistic getStatistics() {
        return ts.getStatistic();
    }
}
