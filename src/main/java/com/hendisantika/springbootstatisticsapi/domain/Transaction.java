package com.hendisantika.springbootstatisticsapi.domain;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-statistics-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/01/20
 * Time: 18.46
 */
public class Transaction implements Comparable<Transaction> {

    private double amount;
    private long timestamp;

    public Transaction() {
    }

    public Transaction(double amount, long timestamp) {
        this.amount = amount;
        this.timestamp = timestamp;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "Transaction{" + "amount=" + amount + ", timestamp=" + timestamp + '}';
    }

    @Override
    public int compareTo(Transaction t) {
        if (t.getTimestamp() == getTimestamp()) {
            return 0;
        } else if (t.getTimestamp() > getTimestamp()) {
            return -1;
        } else {
            return 1;
        }
    }

}