package com.hendisantika.springbootstatisticsapi.service;

import com.hendisantika.springbootstatisticsapi.common.TransactionExpired;
import com.hendisantika.springbootstatisticsapi.domain.Statistic;
import com.hendisantika.springbootstatisticsapi.domain.Transaction;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-statistics-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/01/20
 * Time: 18.48
 */
public interface TransactionService {

    void add(Transaction t) throws TransactionExpired;

    Statistic getStatistic();

    void removeExpired();

    boolean isExpired(Transaction t);
}